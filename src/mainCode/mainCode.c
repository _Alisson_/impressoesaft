#include "queue.h"
#include <stdbool.h>
#include <stdio.h>
#include "printing.h"

#define MAXQUEUE 10
#define OUTPUTFOLDER "../../output/"

int main() {
  Queue *printQueue = queue_create();
  char returnString[2000] = "";
  // setup

  while (printQueue->size < 10) {
    // receber requisicoes e guardar no fila
    queue_push(printQueue, "jorge", "oi", "10/04/1972");
  }
  // escrever impressoes em um arquivo esvaziando a fila
  queue_list(printQueue, returnString);
  printf("%s", returnString);
  printQueueToFile(printQueue);

}

#include "queue.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool printQueueToFile(Queue *toPrint) {

  Node *lastElement = toPrint->end;
  FILE *output;
  output = fopen("../../output/print.txt", "a");
  if (output) {
    while (toPrint->size > 0) {
      Node *line = queue_dequeue(toPrint);
      fprintf(output, "%s, %s, %s;\n", line->name, line->info, line->date);
      free(line);
    }
  } else
    return false;

  return true;
}

void queue_list(Queue *queue, char *returnString) {
  if (queue_isEmpty(queue)) {
    printf("stack vazio!\n");
    return;
  }

  Node *aux = queue->begin;
  while (aux != NULL) {
    char info[2000];
    sprintf(info, "%s, %s, %s;\n", aux->name, aux->info, aux->date);
    strcat(returnString, info);
    if(aux->next == NULL){
      return;
    }

    aux = aux->next;
  }
}
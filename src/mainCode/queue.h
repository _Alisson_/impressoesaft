#pragma once
#include <stdbool.h>

typedef struct node Node;
typedef struct queue {
  Node *begin;
  Node *end;
  int size;
} Queue;

typedef struct node {
  int data;
  char name[50];
  char info[100];
  char date[100];

  struct node *next;
} Node;

Queue* queue_create();
void queue_destroy(Queue** queue);
void queue_enqueue(Queue* queue, int data);
Node *queue_dequeue(Queue* queue);
bool queue_isEmpty(Queue* queue);
int queue_peek(Queue* queue);
void queue_push(Queue *queue, char *name, char *info, char *date);

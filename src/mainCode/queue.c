#include "queue.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Node *node_create() {
  Node *newNode = (Node *)calloc(1, sizeof(Node));
  if (newNode != NULL) {
    newNode->next = NULL;
  }

  return newNode;
}

Queue *queue_create() {
  Queue *newList = (Queue *)calloc(1, sizeof(Queue));

  if (newList != NULL) {
    newList->begin = NULL;
    newList->end = NULL;
    newList->size = 0;
  } else {
    // error
  }

  return newList;
}

bool queue_isEmpty(Queue *queue) {
  if (queue->begin == NULL) {
    return true;
  } else {
    return false;
  }
}

void queue_enqueue(Queue *queue, int data) {
  Node *newNode = node_create();
  newNode->data = data;

  if (queue_isEmpty(queue)) {
    queue->begin = newNode;
    queue->end = newNode;
    newNode->next = NULL;
  } else {
    newNode->next = NULL;
    Node *lastNode = queue->end;
    lastNode->next = newNode;
    queue->end = newNode;
  }

  queue->size++;
}

void queue_push(Queue *queue, char *name, char *info, char *date) {
  Node *newNode = node_create();
  strcpy(newNode->name, name);
  strcpy(newNode->info, info);
  strcpy(newNode->date, date);

  if (queue_isEmpty(queue)) {
    queue->begin = newNode;
    queue->end = newNode;
    newNode->next = NULL;
  } else {
    newNode->next = NULL;
    Node *lastNode = queue->end;
    lastNode->next = newNode;
    queue->end = newNode;
  }

  queue->size++;
}
Node *queue_dequeue(Queue *queue) {
  Node *newBegin = queue->begin->next;
  Node *oldBegin = queue->begin;

  queue->begin = newBegin;
  queue->size--;
  return oldBegin;
}

int queue_peek(Queue *queue) {
  Node *firstIn = queue->begin;
  return firstIn->data;
}

void queue_destroy(Queue **queue) {
  Queue *q = *queue;

  while (!queue_isEmpty(q)) {
    queue_dequeue(q);
  }

  free(q);
  *queue = NULL;
}